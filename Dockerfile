FROM nginx:1.18.0-alpine

USER 0
RUN rm -rf /var/www && \
    rm -rf /etc/nginx

COPY nginx /etc/nginx
COPY www /var/www
