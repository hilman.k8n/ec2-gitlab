## Prerequisite
Make sure to set these environment variables in Settings -> CI/CD -> Variables
- ```AWS_ACCESS_KEY_ID```

  AWS access key ID to acces AWS API
- ```AWS_SECRET_ACCESS_KEY```

  AWS secret key access to acces AWS API
- ```AWS_DEFAULT_REGION```
  
  AWS region where this pipeline will run

- ```DOCKER_USERNAME```

  docker registry username
- ```DOCKER_PASSWORD```

  docker registry password


## Pipeline

- build

  Every code commited to a branch (except master), will trigger build-docker

    build docker image based on nginx:1.18.0-alpine image with root /var/www 
- deploy

  Every code merged to master, deploy stage will create ec2 instance and installing docker then run nginx container with image that was created on build-docker stage

## Result

![result](result.png)

